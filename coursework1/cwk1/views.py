from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse, HttpResponseBadRequest
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
import datetime
from .models import NewsStories, Author
import json

# Create your views here.
@csrf_exempt
def LogInRequest(request):
    http_bad_response = HttpResponseBadRequest()
    http_bad_response['Content-Type'] = 'text/plain'

    if(request.method != 'POST'):
        http_bad_response.status_code = 200
        http_bad_response.reason_phrase = 'Bad Request'
        http_bad_response.content = "Only POST requests allowed\n"
        return http_bad_response

    username = request.POST.get('username')
    password = request.POST.get('password')
    user = authenticate(request, username=username, password=password)
    http_response = HttpResponse()
    http_response['Content-Type'] = 'text/plain'
    if user is not None:
        login(request, user)
        http_response.content = "Welcome!!\n"
        http_response.status_code = 200
        http_response.reason_phrase = 'OK'
        is_user_logged = user.is_authenticated
    else:
        http_response.content = "ERROR: Can't log in\n"
        http_response.status_code = 401
        http_response.reason_phrase = 'Unauthorized'

    return http_response

@csrf_exempt
def LogOutRequest(request):
    http_bad_response = HttpResponseBadRequest()
    http_bad_response['Content-Type'] = 'text/plain'

    if(request.method != 'POST'):
        http_bad_response.status_code = 200
        http_bad_response.reason_phrase = 'Bad Request'
        http_bad_response.content = "Only POST requests allowed\n"
        return http_bad_response

    logout(request)
    if(request.user.is_authenticated == False):
        http_response = HttpResponse()
        http_response['Content-Type'] = 'text/plain'
        http_response.content = "Goodbye!!\n"
        http_response.status_code = 200
        http_response.reason_phrase = 'OK'
    else:
        http_bad_response.status_code = 503
        http_bad_response.reason_phrase = 'Service Unavailable'
        return http_bad_response

    return http_response

@csrf_exempt
def PostStoryRequest(request):
    http_bad_response = HttpResponseBadRequest()
    http_bad_response['Content-Type'] = 'text/plain'
    http_response = HttpResponse()
    http_response['Content-Type'] = 'text/plain'

    if(request.method != 'POST'):
        http_bad_response.status_code = 200
        http_bad_response.reason_phrase = 'Bad Request'
        http_bad_response.content = "Only POST requests allowed\n"
        return http_bad_response

    if(request.user.is_authenticated):
        body = json.loads(request.body)
        headline = body['headline']
        category = body['category']
        region = body['region']
        details = body['details']
        userPk = User.objects.get(username=request.user).id
        author_instance = Author.objects.get(users_id = userPk)

        new_story = NewsStories(headline=headline, story_category=category, story_region=region, story_author=author_instance, story_details=details)
        new_story.save()
        http_response.status_code = 201
        http_response.reason_phrase = 'CREATED'
        return http_response
    else:
        http_response.content = "ERROR: Not Signed In\n"
        http_response.status_code = 401
        http_response.reason_phrase = 'Unauthorized'
        return http_response

@csrf_exempt
def GetStoriesRequest(request):
        http_bad_response = HttpResponseBadRequest()
        http_bad_response['Content-Type'] = 'text/plain'

        if(request.method != 'GET'):
            http_bad_response.status_code = 200
            http_bad_response.reason_phrase = 'Bad Request'
            http_bad_response.content = "Only GET requests allowed\n"
            return http_bad_response

        body = json.loads(request.body)
        category = body['story_cat']
        region = body['story_region']
        date = body['story_date']
        stories = NewsStories.objects.all().values('id', 'headline', 'story_category', 'story_region', 'story_author', 'story_date', 'story_details')

        if stories == None:
            http_bad_response.status_code = 404
            http_bad_response.reason_phrase = 'Not Found'
            http_bad_response.content = "No stories can be found\n"
            return http_bad_response

        enddate = datetime.date.today()

        if(category != '*'):
            stories = stories.filter(story_category=category)
        if(region != '*'):
            stories = stories.filter(story_region=region)
        if(date != '*'):
            stories = stories.filter(story_date__gte=date)

        data_list = []
        for element in stories:
            item = {'key': element['id'], 'headline': element['headline'], 'story_cat': element['story_category'],
            'story_region': element['story_region'], 'author': Author.objects.get(users_id=element['story_author']).author_name,
            'story_date': str(element['story_date']), 'story_details': element['story_details']}
            data_list.append(item)

        payload = {'stories': data_list}

        http_response = HttpResponse(json.dumps(payload))
        http_response['Content-Type'] = 'application/json'
        http_response.status_code = 200
        http_response.reason_phrase = 'OK'
        return http_response

@csrf_exempt
def DeleteStoryRequest(request):
    http_bad_response = HttpResponseBadRequest()
    http_bad_response['Content-Type'] = 'text/plain'
    http_response = HttpResponse()
    http_response['Content-Type'] = 'text/plain'

    if(request.method != 'POST'):
        http_bad_response.status_code = 200
        http_bad_response.reason_phrase = 'Bad Request'
        http_bad_response.content = "Only POST requests allowed\n"
        return http_bad_response
    try:
        id_to_delete = request.POST.get('story_key')

        NewsStories.objects.filter(id=id_to_delete).delete()

        http_response.status_code = 201
        http_response.reason_phrase = 'CREATED'
        return http_response
    except:
        http_bad_response.status_code = 503
        http_bad_response.reason_phrase = 'Service Unavailable'
        return http_bad_response
