from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Author(models.Model):
    users = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    author_name = models.CharField(max_length=64, default='Default')

class NewsStories(models.Model):
    headline = models.CharField(max_length=64)
    story_category = models.CharField(max_length=8)
    story_region = models.CharField(max_length=4)
    story_author = models.ForeignKey(Author, on_delete=models.CASCADE)
    story_date = models.DateField(auto_now_add=True)
    story_details = models.TextField(max_length=512)
